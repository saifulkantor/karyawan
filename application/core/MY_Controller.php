<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		if (isset($this->session->id_user) == false or $this->session->id_user < 0) {
			redirect('Auth');
		} else {
			// cek auth key
			if (!password_verify($this->session->id_user.$this->session->timelogin, $this->session->authkey)) {
				/* decide what the content should be up here .... */
				//$content = get_content(); //generic function;

				/* AJAX check  */
				if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
					/* special ajax here */
					if (isset($_REQUEST['contentType']) && $_REQUEST['contentType'] == 'json') {
						die(json_encode(generateMessage(false, 'Auth Key User Tidak Sama<br>Silahkan Login Ulang!<br>', 'Error', 'error')));
					} else {
						die('<script>alert("Auth Key User Tidak Sama\nSilahkan Login Ulangs!")</script>');
					}
				} else {
					if (isset($_REQUEST['contentType']) && $_REQUEST['contentType'] == 'json') {
						die(json_encode(generateMessage(false, 'Auth Key User Tidak Sama<br>Silahkan Login Ulang!<br>', 'Error', 'error')));
					} else {
						redirect('Auth/logout');
						die('<script>alert("Auth Key User Tidak Sama\nSilahkan Login Ulang!"); </script>');
					}
				}
			}
		}
	}
	public function loadview($halaman='index',$data='') {
		$this->load->view('layout',['halaman'=>$halaman,'data'=>$data]);
	}

}
