<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model{
  public $table = '';
	public $id = '';
  public $kode = '';
  public $kolom = array();
	public $table_detail = '';

    function __construct()
	{
        parent::__construct();
    }
    public function get($id){
  		$sql = "select a.*
  				from {$this->table} a
  				where a.{$this->id} = ".$id;
  		$query = $this->db->query($sql, $id);
  		if ($query) {
  			$msg = generateMessage(true);
  			$msg['data'] = $query->row();
  			return $msg;
  		} else {
  			$err = $this->db->error();
  			return generateMessage(false, $err['message'], 'Peringatan', 'error');
  		}
  	}

    public function masukkandata($datainput) {
  		$data = array();
  		foreach ($this->kolom as $key => $kolom) {
  			$data[$key]=$datainput[$key]??$kolom;
  		}
  		return $data;
  	}

  	public function getAll(){
  		$sql = "select a.*
  				from {$this->table} a";
  		$query = $this->db->query($sql);
  		if ($query) {
  			$msg = generateMessage(true);
  			$msg['data'] = $query->result_array();
  			return $msg;
  		} else {
  			$err = $this->db->error();
  			return generateMessage(false, $err['message'], 'Peringatan', 'error');
  		}
  	}


    	public function getDuplicate($kode,$id_asli=0){
    		$sql = "select {$this->id} as id
    				from {$this->table}
    				where {$this->kode} = '".$kode."'".(($id_asli>0)?" and {$this->id} != ".$id_asli:"");
    		$query = $this->db->query($sql,$kode);
    		$row = $query->row();
    		return $row->id ?? null;
    	}

      public function save($data=null,$datatambahan=null) {
        // start trans
        $this->db->trans_begin();
        $id = $data[$this->id];
        unset($data[$this->id]);
        if ($id<=0) {
          $sql = generateSqlInsert($this->table, $data);
        } else {
          $sql = generateSqlUpdate($this->table, $data, array($this->id=>$id));
        }
        $query = $this->db->query($sql['sql'], $sql['param']);
        if ($id<=0) {
          $query2 = $this->db->query('select @@IDENTITY as last_id');
          $id = $query2->row()->last_id;
        }

        if ($this->db->trans_status() === FALSE) {
          // rollback
          $this->db->trans_rollback();

          $err = $this->db->error();
          return array_merge(generateMessage(false, $err['message'], 'Peringatan', 'error'),array('id'=>$id));
        } else {
          // commit
          $this->db->trans_commit();

          return array_merge(generateMessage(true),array('id'=>$id));
        }
      }

      public function delete($id){
        $sql = "delete from {$this->table} where {$this->id} = ?";
        $query = $this->db->query($sql, $id);

        if ($query) {
          return generateMessage(true);
        } else {
          $err = $this->db->error();
          return generateMessage(false, $err['message'], 'Error', 'error');
        }
      }
}
