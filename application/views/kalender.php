<div class="row">
   <h1>Kalender</h1>

	<div id="body" style="width:100%">
    <button class="btn btn-primary" onclick="tuabs()"> Tambah Izin </button>
    <div class="totalizin" style="display:none"></div>
    <div id='calendar'></div>
  </div>
</div>

<div class="modal fade" id="popupdetail" role="dialog">
  <div class="modal-dialog">
    <!-- Modal Content -->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 1px solid #e6e6e6;">
        <h4 class="modal-title" style="margin:0">Detail Izin</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <span class="h3 jenis"> </span>
        <br/>
        <p> Tanggal : <span class="start_date"></span> - <span class="end_date"></span> (<span class="diff_date"></span> Hari) </p>
        <label>Keterangan</label>
        <p class="keterangan"></p>
        <button class='btn btn-primary btn-round' name='ok' type="button" lass="close" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="popupabs" role="dialog">
  <div class="modal-dialog">
    <!-- Modal Content -->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 1px solid #e6e6e6;">
        <h4 class="modal-title" style="margin:0">Detail Absen</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <span class="h3 jenis"> </span>
        <br/>
        <p> Tanggal : <span class="tanggal"></span> </p>
        <label>Keterangan</label>
        <p class="keterangan"></p>
        <button class='btn btn-primary btn-round' name='ok' type="button" lass="close" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="popupabsen" role="dialog">
  <div class="modal-dialog">
    <!-- Modal Content -->
    <div class="modal-content">
      <div class="modal-header" style="border-bottom: 1px solid #e6e6e6;">
        <h4 class="modal-title" style="margin:0">Jam Masuk & Pulang</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <span class="h3"><span class="jammasuk"></span> - <span class="jamkeluar"></span> </span>
      </div>
      <div class="modal-footer">
        <button class='btn btn-primary btn-round' name='ok' type="button" lass="close" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="popuptuabs" role="dialog">
  <div class="modal-dialog" style="transform: none;">
    <!-- Modal Content -->
    <div class="modal-content">
      <form action="" method="post" enctype="multipart/form-data">
        <input type="hidden" id="idabs" name="doc_id" value="0" />
        <div class="modal-header" style="border-bottom: 1px solid #e6e6e6;">
          <h4 class="modal-title" style="margin:0">Tambah Izin</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6">
              <input class="form-control" type="date" id="abstanggal" name="tgl" value="" min="" max="" />
            </div>
            <div class="col-md-6">
              <select class="form-control" name="status" id="absstatus">
                <option value="Izin">Izin</option>
                <option value="Sakit">Sakit</option>
                <option value="Dinas Luar">Dinas Luar</option>
              </select>
            </div>
          </div>
          <textarea class="form-control" id="absketerangan" placeholder="Keterangan" name="keterangan" rows="5" required></textarea>
          <img src="<?=base_url()?>assets/images/abs/default.jpg" class="previewimageimgk" style="width:100%;cursor:pointer;" onclick="$('#inputimgk').click()">
          <input type="file" class="inputimage" style="display:none" data-name="imgk" name="image" id="inputimgk" accept="image/*">
        </div>
        <div class="modal-footer">
          <button class='btn btn-primary btn-round' name='simpan' type="submit">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
var monthloaded = new Array();
var monthloaded2 = new Array();
var monthloaded3 = new Array();
var dataleave = new Array();
var dataabsen = new Array();
var dataabs = new Array();
function getevents(month,year){
  if($.inArray(month+'-'+year,monthloaded)<0){
    var request = $.ajax({
      url: "ajax/kalender_izin",
      method: "POST",
      data: {
        'month': month,
        'year': year,
      },
      dataType: "json"
    });

    request.done(function( datahasil ) {
      var totalizin = 0;
      $.each(datahasil,function(index,data){
        if (dataleave.findIndex(x => x.id == data["id"])<0) {
          var newevent = {
            id: data["jenis"]+'-'+data["id"],
            title: data["keterangan"],
            textEscape: false,
            start: data["start_date"],
            end: data["end_date"],
            className: 'primary',
            url: 'javascript:detailload('+data["id"]+');'
          };
          var start_date = new Date(data["start_date"]);
          var end_date = new Date(data["end_date"]);
          totalizin+=jmlHariKerja(start_date,end_date);
          $('#calendar').fullCalendar('renderEvent', newevent, true);
          dataleave.push(data);
        }
      });
      $('.totalizin').html(totalizin+' Hari');
      monthloaded.push(month+'-'+year);
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
  }
}
function getdataabsen(month,year){
  if($.inArray(month+'-'+year,monthloaded2)<0){
    var d = new Date(year, month, 0);
    var mulai = year+'-'+month+'-1';
    var akhir = year+'-'+month+'-'+d.getDate();
      var request = $.ajax({
        url: "ajax/dataabsen",
        method: "POST",
        data: {
          'mulai': mulai,
          'akhir': akhir,
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        $.each(datahasil,function(index,data){
          data["On_Duty"]=(data["On_Duty"]!=null)?data["On_Duty"]:0;
          data["Off_Duty"]=(data["Off_Duty"]!=null)?data["Off_Duty"]:0;
          if (dataabsen.findIndex(x => x.Tgl == data["Tgl"])<0 && (data["On_Duty"]!=0 || data["Off_Duty"]!=0)) {
            var tgl = new Date(data["Tgl"]);
            tgl = tgl.getFullYear()+'-'+(tgl.getMonth()+1)+'-'+tgl.getDate();
            var newevent = {
              id: 'absen-'+data["Tgl"],
              title: data["On_Duty"].toString().substr(0,5)+' - '+data["Off_Duty"].toString().substr(0,5),
              textEscape: false,
              start:tgl,
              end: tgl,
              className: 'info',
              url: 'javascript:detailabsen(\''+data["Tgl"]+'\');'
            };
            $('#calendar').fullCalendar('renderEvent', newevent, true);
            dataabsen.push(data);
          }
        });
        monthloaded2.push(month+'-'+year);
        getdataabs(month,year);
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
      });
  }
}
function getdataabs(month,year){
  if($.inArray(month+'-'+year,monthloaded3)<0){
    var d = new Date(year, month, 0);
    var mulai = year+'-'+month+'-1';
    var akhir = year+'-'+month+'-'+d.getDate();
      var request = $.ajax({
        url: "ajax/dataabs",
        method: "POST",
        data: {
          'mulai': mulai,
          'akhir': akhir,
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        console.log(datahasil);
        $.each(datahasil,function(index,data){
          if (dataabs.findIndex(x => x.id == data["doc_id"])<0) {
            var tgl = new Date(data["tgl"]);
            tgl = tgl.getFullYear()+'-'+(tgl.getMonth()+1)+'-'+tgl.getDate();
            var newevent = {
              id: 'absen-'+tgl,
              title: data["status"],
              textEscape: false,
              start:tgl,
              end: tgl,
              className: 'important',
              url: 'javascript:detailabs('+data["doc_id"]+');'
            };
            $('#calendar').fullCalendar('renderEvent', newevent, true);
            dataabs.push(data);
          }
        });
        monthloaded3.push(month+'-'+year);
        //getevents(month,year);
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
      });
  }
}
window.addEventListener('DOMContentLoaded', (event) => {
  var calendar =  $('#calendar').fullCalendar({
    header: {
      left: 'title',
      center: '', //'agendaDay,agendaWeek,month'
      right: 'prev,next today'
    },
    editable: false,
    firstDay: 0, //  1(Monday) this can be changed to 0(Sunday) for the USA system
    selectable: false,
    defaultView: 'month',

    axisFormat: 'h:mm',
    columnFormat: {
              month: 'ddd',    // Mon
              week: 'ddd d', // Mon 7
              day: 'dddd M/d',  // Monday 9/7
              agendaDay: 'dddd d'
          },
          titleFormat: {
              month: 'MMMM yyyy', // September 2009
              week: "MMMM yyyy", // September 2009
              day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
          },
    allDaySlot: false,
    selectHelper: false,
    select: function(start, end, allDay) {
      var title = prompt('Event Title:');
      if (title) {
        calendar.fullCalendar('renderEvent',
          {
            title: title,
            start: start,
            end: end,
            allDay: allDay
          },
          true // make the event "stick"
        );
      }
      calendar.fullCalendar('unselect');
    },
    droppable: false, // this allows things to be dropped onto the calendar !!!
    // drop: function(date, allDay) { // this function is called when something is dropped
    //
    //   // retrieve the dropped element's stored Event Object
    //   var originalEventObject = $(this).data('eventObject');
    //
    //   // we need to copy it, so that multiple events don't have a reference to the same object
    //   var copiedEventObject = $.extend({}, originalEventObject);
    //
    //   // assign it the date that was reported
    //   copiedEventObject.start = date;
    //   copiedEventObject.allDay = allDay;
    //
    //   // render the event on the calendar
    //   // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
    //   $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
    //
    //   // is the "remove after drop" checkbox checked?
    //   if ($('#drop-remove').is(':checked')) {
    //     // if so, remove the element from the "Draggable Events" list
    //     $(this).remove();
    //   }
    //
    // },

    events: [

    ],
    eventRender: function (event, element) {
        element.find('.fc-event-title').html(event.title);
        var totalizin =0;
        var start_date = new Date(event.start);
        var end_date = new Date(event.end);
        totalizin+=jmlHariKerja(start_date,end_date);
        $('.totalizin').html(totalizin+' Hari');
    },
    viewRender: function(view, element) {
      $('.totalizin').html(0+' Hari');
      var b = $('#calendar').fullCalendar('getDate');
      getdataabsen(b.getMonth()+1,b.getFullYear());
      var d = new Date(b.getFullYear(), b.getMonth()+1, 0);
      var mulai = d.getFullYear()+'-'+("0" + (d.getMonth() + 1)).slice(-2)+'-01';
      var akhir = d.getFullYear()+'-'+("0" + (d.getMonth() + 1)).slice(-2)+'-'+d.getDate();
      $('#abstanggal').attr('min',mulai);
      $('#abstanggal').attr('max',akhir);
      $('#abstanggal').val(mulai);
    }
  });
  <?php if (isset($_POST['tgl'])) {
  echo "$('#calendar').fullCalendar( 'gotoDate', new Date('".$_POST['tgl']."'));";
  } ?>

});
function detailload(id) {
  var idx = dataleave.findIndex(x => x.id == id);
  var start_date = new Date(dataleave[idx]['start_date']);
  var end_date = new Date(dataleave[idx]['end_date']);
  var diff_date = jmlHariKerja(start_date,end_date);
  end_date = end_date.getDate()+' '+monthNames[end_date.getMonth()]+' '+end_date.getFullYear();
  start_date = start_date.getDate()+' '+monthNames[start_date.getMonth()]+' '+start_date.getFullYear();
  $('#popupdetail .modal-body .jenis').html(dataleave[idx]['jenis']);
  $('#popupdetail .modal-body .start_date').html(start_date);
  $('#popupdetail .modal-body .end_date').html(end_date);
  $('#popupdetail .modal-body .diff_date').html(diff_date);
  $('#popupdetail .modal-body .keterangan').html(dataleave[idx]['keterangan']);
  $('#popupdetail').modal();
}
function detailabsen(Tgl) {
  var idx = dataabsen.findIndex(x => x.Tgl == Tgl);
  var Tgl = new Date(dataabsen[idx]["Tgl"]);
  Tgl = Tgl.getDate()+' '+monthNames[Tgl.getMonth()]+' '+Tgl.getFullYear()+' ('+dayNames[Tgl.getDay()]+')';
  $('#popupabsen .modal-title').html(Tgl);
  $('#popupabsen .modal-body .jammasuk').html(dataabsen[idx]["On_Duty"].toString().substr(0,5));
  $('#popupabsen .modal-body .jamkeluar').html(dataabsen[idx]["Off_Duty"].toString().substr(0,5));
  $('#popupabsen').modal();
}
function detailabs(doc_id) {
  var idx = dataabs.findIndex(x => x.doc_id == doc_id);
  if (dataabs[idx]["approve"]==0) {
    tuabs(doc_id);
  } else if (dataabs[idx]["approve"]==1) {
    var tgl = new Date(dataabs[idx]["tgl"]);
    tgl = tgl.getDate()+' '+monthNames[tgl.getMonth()]+' '+tgl.getFullYear()+' ('+dayNames[tgl.getDay()]+')';
    $('#popupabs .modal-body .jenis').html(dataabs[idx]['status']);
    $('#popupabs .modal-body .tanggal').html(tgl);
    $('#popupabs .modal-body .keterangan').html(dataabs[idx]['keterangan']);
    $('#popupabs').modal();
  }
}
function tuabs(doc_id=0){
  var tgl=new Date($('#abstanggal').attr('min'));
  var status='Izin';
  var keterangan='';
  if (doc_id!=0){
    var idx = dataabs.findIndex(x => x.doc_id == doc_id);
    tgl = new Date(dataabs[idx]["tgl"]);
    status = dataabs[idx]["status"];
    keterangan = dataabs[idx]["keterangan"];
    $('#popuptuabs .modal-header').html('Ubah Izin');
    gantiimages('.previewimageimgk','<?=base_url()?>assets/images/abs/'+doc_id+'.jpg','<?=base_url()?>assets/images/abs/default.jpg');
  } else {
    $('#popuptuabs .modal-header').html('Tambah Izin');
    $('.previewimageimgk').attr('src','<?=base_url()?>assets/images/abs/default.jpg');
  }
  $('#idabs').val(doc_id);
  tgl = tgl.getFullYear()+'-'+("0" + (tgl.getMonth() + 1)).slice(-2)+'-'+("0" + tgl.getDate()).slice(-2);
  $('#popuptuabs #absstatus').val(status);
  $('#popuptuabs #abstanggal').val(tgl);
  $('#popuptuabs #absketerangan').html(keterangan);
  $('#popuptuabs').modal();
}
</script>
