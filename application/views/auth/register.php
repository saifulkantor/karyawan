<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$status_pegawai = array(
  ['id'=>'Tetap','keterangan'=>'Tetap'],
  ['id'=>'Kontrak','keterangan'=>'Kontrak'],
  ['id'=>'OutSourcing','keterangan'=>'OutSourcing'],
  ['id'=>'Casual','keterangan'=>'Casual'],
);
$_GET['tab'] = $_GET['tab']??'pribadi';
?>
<div class="row">
  <form action="" method="POST" enctype="multipart/form-data" style="width:100%">
	   <h1 class="pull-left">Isi data kamu</h1>
     <div class="pull-right" style="width: 10%;min-width: 100px;">
       <img src="<?=($_POST['linkimage'])??''?>" class="previewimageimgk" style="width:100%;cursor:pointer;" onclick="$('#inputimgk').click()">
       <input type="file" class="inputimage" style="display:none" data-name="imgk" name="image" id="inputimgk" accept="image/*">
     </div>
     <div style="clear:both"></div>
  	<div id="body" style="width:100%">
          <ul class="nav nav-tabs" id="datakaryawantab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pribadi-tab" data-toggle="tab" href="#pribadi" role="tab" aria-controls="pribadi" aria-selected="true">Data Pribadi</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="pendidikan-tab" data-toggle="tab" href="#pendidikan" role="tab" aria-controls="pendidikan" aria-selected="false">Data Pendidikan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="keluarga-tab" data-toggle="tab" href="#keluarga" role="tab" aria-controls="keluarga" aria-selected="false">Data Keluarga</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="akun-tab" data-toggle="tab" href="#akun" role="tab" aria-controls="akun" aria-selected="false">Akun</a>
            </li>
          </ul>
          <div class="tab-content" id="datakaryawantabContent" style="padding:20px 0px">
          <div class="tab-pane fade show active" id="pribadi" role="tabpanel" aria-labelledby="pribadi-tab">
            <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nama</label>
                <input type="text" class="form-control" name="nama_karyawan" value="<?=($_POST['nama_karyawan'])??''?>" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" >
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tgl Lahir</label>
                <input type="date" class="form-control" name="tgl_lahir" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" value="<?=(date_format(date_create($_POST['tgl_lahir']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Gender</label>
                <select name="gender" class="form-control">
                    <option value="Laki-Laki">Laki-Laki</option>
                    <option value="Perempuan">Perempuan</option>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Agama</label>
                <select name="agama" class="form-control">
                    <option value="Islam">Islam</option>
                    <option value="Kristen">Kristen</option>
                    <option value="Hindu">Hindu</option>
                    <option value="Budha">Budha</option>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Marital</label>
                <select name="status" class="form-control">
                    <option value="Lajang">Lajang</option>
                    <option value="Menikah">Menikah</option>
                    <option value="Duda">Duda</option>
                    <option value="Janda">Janda</option>
                  </select>
              </div>
            </div>
            <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">NiK</label>
                  <input type="text" class="form-control" name="nik" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" value="<?=($_POST['nik'])??''?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">NPWP</label>
                  <input type="text" class="form-control" name="npwp" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" value="<?=($_POST['npwp'])??''?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">NO KK</label>
                  <input type="text" class="form-control" name="kk" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" value="<?=($_POST['kk'])??''?>">
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Alamat</label>
                  <textarea type="text" rows="5" class="form-control" name="alamat"><?=($_POST['alamat'])??''?></textarea>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">Kota</label>
                  <input type="text" class="form-control" name="kota" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" value="<?=($_POST['kota'])??''?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">No Telp</label>
                  <input type="text" class="form-control" name="telp" required oninvalid="pilihtabinputkosong('pribadi-tab',this)" value="<?=($_POST['telp'])??''?>">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group bmd-form-group">
                  <label class="bmd-label-floating">FP Code</label>
                    <input type="text" class="form-control" name="code" value="<?=($_POST['code'])??''?>">
                  </div>
              </div>
              <?php if (isset($this->session->id_user) == true and $this->session->id_user >= 0) {
                $target_path = BASEPATH.'../assets/dokumen/karyawan/pribadi/'.$this->session->id_user.'/';
                if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
                $filecount = 0;
                $files = glob($target_path . "*");
                $filedokpribadi = $files;
                $htmlfiles='';
                foreach ($files as $key => $value) {
                  $htmlfiles.='<div class="col-md-4 dokpribadi dokpribadi'.$key.'">
                    <div class="form-group bmd-form-group">
                      <i class="fa fa-close hapusdokumen" onclick="hapusdokpribadi('.$key.')"></i>
                      <a href="'.base_url().'assets/dokumen/karyawan/pribadi/'.$this->session->id_user.'/'.basename($value).'" target="_blank" class="alert alert-primary" style="display: block;">'.basename($value).'</a>
                    </div>
                  </div>';
                }
                echo '<div class="col-md-12"><label class="bmd-label-floating">Dokumen</label></div>
                '.$htmlfiles.'
                <div class="col-md-4 dokpribadiplus">
                  <div class="form-group bmd-form-group">
                    <span class="alert alert-danger" style="cursor:pointer;display: block;" onclick="browsedokpribadi()"> + Dokumen</span>
                    <input type="file" class="form-control dokpribadi1" name="dokpribadi1" style="display:none" onchange="tambahdokpribadi(1)">
                    <input type="file" class="form-control dokpribadi2" name="dokpribadi2" style="display:none" onchange="tambahdokpribadi(2)">
                    <input type="file" class="form-control dokpribadi3" name="dokpribadi3" style="display:none" onchange="tambahdokpribadi(3)">
                    <input type="file" class="form-control dokpribadi4" name="dokpribadi4" style="display:none" onchange="tambahdokpribadi(4)">
                    <input type="file" class="form-control dokpribadi5" name="dokpribadi5" style="display:none" onchange="tambahdokpribadi(5)">
                  </div>
                </div>
                <input type="hidden" name="listdokpribadihapus" class="listdokpribadihapus" />';
              } ?>
            </div>
          </div>
          <div class="tab-pane fade" id="pendidikan" role="tabpanel" aria-labelledby="pendidikan-tab">
            <div class="row">
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Kota</label>
                <input type="text" class="form-control" name="kota_asal" required oninvalid="pilihtabinputkosong('pendidikan-tab',this)" value="<?=($_POST['kota_asal'])??''?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Pendidikan</label>
                <input type="text" class="form-control" name="jenjang" required oninvalid="pilihtabinputkosong('pendidikan-tab',this)" value="<?=($_POST['jenjang'])??''?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Institusi</label>
                <input type="text" class="form-control" name="Institusi" required oninvalid="pilihtabinputkosong('pendidikan-tab',this)" value="<?=($_POST['Institusi'])??''?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Jurusan</label>
                <input type="text" class="form-control" name="jurusan" required oninvalid="pilihtabinputkosong('pendidikan-tab',this)" value="<?=($_POST['jurusan'])??''?>">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nilai</label>
                <input type="text" class="form-control" name="nilai" required oninvalid="pilihtabinputkosong('pendidikan-tab',this)" value="<?=($_POST['nilai'])??''?>">
              </div>
            </div>
            <?php if (isset($this->session->id_user) == true and $this->session->id_user >= 0) {
              $target_path = BASEPATH.'../assets/dokumen/karyawan/pendidikan/'.$this->session->id_user.'/';
              if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
              $filecount = 0;
              $files = glob($target_path . "*");
              $filedokpendidikan = $files;
              $htmlfiles='';
              foreach ($files as $key => $value) {
                $htmlfiles.='<div class="col-md-4 dokpendidikan dokpendidikan'.$key.'">
                  <div class="form-group bmd-form-group">
                  <i class="fa fa-close hapusdokumen" onclick="hapusdokpendidikan('.$key.')"></i>
                    <a href="'.base_url().'assets/dokumen/karyawan/pendidikan/'.$this->session->id_user.'/'.basename($value).'" target="_blank" class="alert alert-primary" style="display: block;">'.basename($value).'</a>
                  </div>
                </div>';
              }

              echo '<div class="col-md-12"><label class="bmd-label-floating">Dokumen</label></div>
              '.$htmlfiles.'
              <div class="col-md-4 dokpendidikanplus">
                <div class="form-group bmd-form-group">
                  <span class="alert alert-danger" style="cursor:pointer;display: block;" onclick="browsedokpendidikan()"> + Dokumen</span>
                  <input type="file" class="form-control dokpendidikan1" name="dokpendidikan1" style="display:none" onchange="tambahdokpendidikan(1)">
                  <input type="file" class="form-control dokpendidikan2" name="dokpendidikan2" style="display:none" onchange="tambahdokpendidikan(2)">
                  <input type="file" class="form-control dokpendidikan3" name="dokpendidikan3" style="display:none" onchange="tambahdokpendidikan(3)">
                  <input type="file" class="form-control dokpendidikan4" name="dokpendidikan4" style="display:none" onchange="tambahdokpendidikan(4)">
                  <input type="file" class="form-control dokpendidikan5" name="dokpendidikan5" style="display:none" onchange="tambahdokpendidikan(5)">
                </div>
              </div>
              <input type="hidden" name="listdokpendidikanhapus" class="listdokpendidikanhapus" />';
            } ?>
          </div>
          </div>
          <div class="tab-pane fade" id="keluarga" role="tabpanel" aria-labelledby="keluarga-tab">
            <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nama Pasangan</label>
                <input type="text" class="form-control" name="nama_pasangan" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=($_POST['nama_pasangan'])??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tgl Lahir Pasangan</label>
                <input type="date" class="form-control" name="tgl_lahir_pasangan" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=(date_format(date_create($_POST['tgl_lahir_pasangan']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="anak1 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nama Anak 1</label>
                <input type="text" class="form-control" name="anak1" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=($_POST['anak1'])??''?>">
              </div>
            </div>
            <div class="anak1 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tgl Lahir Anak 1</label>
                <input type="date" class="form-control" name="tgl1" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=(date_format(date_create($_POST['tgl1']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="anak2 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nama Anak 2</label>
                <input type="text" class="form-control" name="anak2" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=($_POST['anak2'])??''?>">
              </div>
            </div>
            <div class="anak2 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tgl Lahir Anak 2</label>
                <input type="date" class="form-control" name="tgl2" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=(date_format(date_create($_POST['tgl2']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="anak3 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nama Anak 3</label>
                <input type="text" class="form-control" name="anak3" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=($_POST['anak3'])??''?>">
              </div>
            </div>
            <div class="anak3 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tgl Lahir Anak 3</label>
                <input type="date" class="form-control" name="tgl3" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=(date_format(date_create($_POST['tgl3']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="anak3 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Nama Ibu</label>
                <input type="text" class="form-control" name="nama_ibu" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=($_POST['nama_ibu'])??''?>">
              </div>
            </div>
            <div class="anak3 col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tgl Ibu</label>
                <input type="date" class="form-control" name="tgl4" required oninvalid="pilihtabinputkosong('keluarga-tab',this)" value="<?=(date_format(date_create($_POST['tgl4']),'Y-m-d'))??''?>">
              </div>
            </div>
          </div>
        </div>

          <div class="tab-pane fade" id="akun" role="tabpanel" aria-labelledby="akun-tab">
            <div class="row">
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Username</label>
                <input type="text" class="form-control" name="username" value="<?=($_POST['username'])??''?>" required oninvalid="pilihtabinputkosong('akun-tab',this)">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Password</label>
                <input type="password" class="form-control" name="password" value="<?=($_POST['password'])??''?>" <?=(isset($this->session->id_user) && $this->session->id_user > -1)?'':'required oninvalid="pilihtabinputkosong(\'akun-tab\',this)"'?>>
              </div>
            </div>
            <?php /*
            <h4 class="col-md-12">Data Pekerjaan</h4>
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Level</label>
                <select name="level" class="form-control">
                  <?php foreach ($levelall['data'] as $key => $level) {
                    $selected = ($_POST['level']==$level['kode'])?' selected':'';
                    echo '<option value="'.$level['kode'].'"'.$selected.'>'.$level['keterangan'].'</option>';
                  } ?>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Unit / Cabang</label>
                <select name="unit" class="form-control">
                    <option value="1">Head Office</option>
                    <option value="2">SunCity Plaza</option>
                    <option value="3">Suncity Festival</option>
                    <option value="3">Suncity Biz</option>
                    <option value="4">Suncity Square</option>
                    <option value="5">Suncity Resident</option>
                    <option value="6">The Sun Hotel Sidoarjo</option>
                    <option value="7">SunCity Plaza Sidoarjo</option>
                    <option value="8">Waterpark Sidoarjo</option>
                    <option value="9">The Sun Hotel Madiun</option>
                    <option value="10">SunCity Plaza Madiun</option>
                    <option value="11">Waterpark Madiun</option>
                    <option value="11">PBI</option>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">NIP</label>
                <input type="text" class="form-control" name="nip" required value="<?=($_POST['nip'])??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="bmd-label-floating">Status Kepegawaian</label>
                <select name="status_pegawai" class="form-control">
                  <?php foreach ($status_pegawai as $key => $status) {
                    $selected = ($_POST['status_pegawai']==$status['id'])?' selected':'';
                    echo '<option value="'.$status['id'].'"'.$selected.'>'.$status['keterangan'].'</option>';
                  } ?>
                  </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">SK</label>
                <input type="text" class="form-control" name="sk" required value="<?=($_POST['sk'])??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Kontrak Ke</label>
                <input type="number" class="form-control" name="[kontrak ke]" min="0" required value="<?=($_POST['kontrak ke'])??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tanggal Mulai</label>
                <input type="date" class="form-control" name="mulai" required value="<?=(date_format(date_create($_POST['mulai']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Tanggal Akhir</label>
                <input type="date" class="form-control" name="akhir" required value="<?=(date_format(date_create($_POST['akhir']),'Y-m-d'))??''?>">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Jabatan</label>
                <input type="text" class="form-control" name="jabatan" value="<?=($_POST['jabatan'])??''?>" required>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group bmd-form-group">
                <label class="bmd-label-floating">Departement</label>
                <input type="text" class="form-control" name="dept" value="<?=($_POST['dept'])??''?>" required>
              </div>
            </div>
            */ ?>
          </div>
          </div>
        </div>
          <button type="submit" class="btn btn-primary pull-right" onclick="simpandata()">Simpan</button>
          <div class="clearfix"></div>
  	</div>
  </form>
</div>
<script type="text/javascript">
var dokpribadi = <?=isset($filedokpribadi)?json_encode($filedokpribadi):'[]'?>;
var dokpendidikan = <?=isset($filedokpendidikan)?json_encode($filedokpendidikan):'[]'?>;
var cntinputkosong = 0;
function browsedokpendidikan() {
  if ($('div.dokpendidikan').length<5){
    if ($('input.dokpendidikan1').val()=='') $('input.dokpendidikan1').click();
    else if ($('input.dokpendidikan2').val()=='') $('input.dokpendidikan2').click();
    else if ($('input.dokpendidikan3').val()=='') $('input.dokpendidikan3').click();
    else if ($('input.dokpendidikan4').val()=='') $('input.dokpendidikan4').click();
    else if ($('input.dokpendidikan5').val()=='') $('input.dokpendidikan5').click();
    else alert('Dokumen Maksimal Hanya 5');
  } else alert('Dokumen Maksimal Hanya 5');
}
function browsedokpribadi() {
  if ($('div.dokpribadi').length<5){
    if ($('input.dokpribadi1').val()=='') $('input.dokpribadi1').click();
    else if ($('input.dokpribadi2').val()=='') $('input.dokpribadi2').click();
    else if ($('input.dokpribadi3').val()=='') $('input.dokpribadi3').click();
    else if ($('input.dokpribadi4').val()=='') $('input.dokpribadi4').click();
    else if ($('input.dokpribadi5').val()=='') $('input.dokpribadi5').click();
    else alert('Dokumen Maksimal Hanya 5');
  } else alert('Dokumen Maksimal Hanya 5');
}
function pilihtabinputkosong(idtab='',elemen) {
  if (cntinputkosong==0) {
    console.log($(elemen).attr('name'));
    $('#'+idtab).click();
    setTimeout(function(){ $(elemen).focus(); }, 1500);
    cntinputkosong++;
  }
}
function tambahdokpribadi(no) {
  html='<div class="col-md-4 dokpribadi filedokpribadi'+no+'">'+
    '<div class="form-group bmd-form-group">'+
      '<i class="fa fa-close hapusdokumen" onclick="hapusfiledokpribadi('+no+')"></i>'+
      '<a href="#" class="alert alert-primary" style="display: block;">'+$('input.dokpribadi'+no).val().substring($('input.dokpribadi'+no).val().lastIndexOf('\\')+1)+'</a>'+
    '</div>'+
  '</div>';
  $(html).insertBefore('.dokpribadiplus');
}
function hapusfiledokpribadi(no) {
  if (confirm('Apakah kamu yakin ingin menghapus dokumen ini ?')) {
    $('.filedokpribadi'+no).remove();
    $('input.dokpribadi'+no).val('');
  }
}
function hapusdokpribadi(no) {
  if (confirm('Apakah kamu yakin ingin menghapus dokumen ini ?')) {
    var valhapus = ($('.listdokpribadihapus').val()!='')?$('.listdokpribadihapus').val()+'||'+$('div.dokpribadi'+no+' a').html():$('div.dokpribadi'+no+' a').html();
    $('.listdokpribadihapus').val(valhapus);
    $('div.dokpribadi'+no).remove();

    //$('.dokpribadi'+no+' .fa.fa-close').remove();
    //$('.dokpribadi'+no).append('<sub class="dokalert alert alert-warning">Dihapus</sub>');
  }
}

function tambahdokpendidikan(no) {
  html='<div class="col-md-4 dokpendidikan filedokpendidikan'+no+'">'+
    '<div class="form-group bmd-form-group">'+
      '<i class="fa fa-close hapusdokumen" onclick="hapusfiledokpendidikan('+no+')"></i>'+
      '<a href="#" class="alert alert-primary" style="display: block;">'+$('input.dokpendidikan'+no).val().substring($('input.dokpendidikan'+no).val().lastIndexOf('\\')+1)+'</a>'+
    '</div>'+
  '</div>';
  $(html).insertBefore('.dokpendidikanplus');
}
function hapusfiledokpendidikan(no) {
  if (confirm('Apakah kamu yakin ingin menghapus dokumen ini ?')) {
    $('.filedokpendidikan'+no).remove();
    $('input.dokpendidikan'+no).val('');
  }
}
function hapusdokpendidikan(no) {
  if (confirm('Apakah kamu yakin ingin menghapus dokumen ini ?')) {
    var valhapus = ($('.listdokpendidikanhapus').val()!='')?$('.listdokpendidikanhapus').val()+'||'+$('div.dokpendidikan'+no+' a').html():$('div.dokpendidikan'+no+' a').html();
    $('.listdokpendidikanhapus').val(valhapus);
    $('div.dokpendidikan'+no).remove();

    //$('.dokpendidikan'+no+' .fa.fa-close').remove();
    //$('.dokpendidikan'+no).append('<sub class="dokalert alert alert-warning">Dihapus</sub>');
  }
}


function simpandata() {
  cntinputkosong = 0;
}

window.addEventListener('DOMContentLoaded', (event) => {
  $('#<?=$_GET['tab']?>-tab').click();
});
</script>
