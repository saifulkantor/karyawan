<div class="row">
    <div class="col-md-6 ml-auto mr-auto">
        <div class="card">
            <div class="header text-center">
                <h4 class="title">Login</h4>
                <br>
            </div>
            <form action="auth/login" method="post" class="container-fluid" style="padding:30px 20px">
              <div class="form-group">
                  <label>Username</label>
                  <input type="text" id="username" name="username" class="form-control" placeholder="Masukkan Username">
              </div>
              <div class="form-group">
                  <label>Password</label>
                  <input type="password" id="password" name="password" class="form-control" placeholder="********">
              </div>
              <button type="submit" target="_blank" class="btn btn-round btn-fill btn-info">Login</button>
            </form>
        </div>
    </div>
</div>
