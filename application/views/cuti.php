<div class="row">
   <h1>Cuti</h1>

	<div id="body" style="width:100%">
    <div class="totalizin" style="display:none"></div>
    <table class="table table-hover table-striped table-bordered table-freeze">
      <thead>
        <tr><th></th></tr>
      </thead>
    </table>
  </div>
</div>

<script type="text/javascript">
var monthloaded = new Array();
var monthloaded2 = new Array();
var dataleave = new Array();
function getevents(month,year){
  if($.inArray(month+'-'+year,monthloaded)<0){
    var request = $.ajax({
      url: "ajax/kalender_izin",
      method: "POST",
      data: {
        'month': month,
        'year': year,
      },
      dataType: "json"
    });

    request.done(function( datahasil ) {
      var totalizin = 0;
      $.each(datahasil,function(index,data){
        if (dataleave.findIndex(x => x.id == data["id"])<0) {
          var newevent = {
            id: data["jenis"]+'-'+data["id"],
            title: data["keterangan"],
            textEscape: false,
            start: data["start_date"],
            end: data["end_date"],
            className: 'primary',
            url: 'javascript:detailload('+data["id"]+');'
          };
          var start_date = new Date(data["start_date"]);
          var end_date = new Date(data["end_date"]);
          totalizin+=jmlHariKerja(start_date,end_date);
          $('#calendar').fullCalendar('renderEvent', newevent, true);
          dataleave.push(data);
        }
      });
      $('.totalizin').html(totalizin+' Hari');
      monthloaded.push(month+'-'+year);
    });

    request.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
  }
}
function getdataabsen(month,year){
  if($.inArray(month+'-'+year,monthloaded2)<0){
    var d = new Date(year, month, 0);
    var mulai = year+'-'+month+'-1';
    var akhir = year+'-'+month+'-'+d.getDate();
      var request = $.ajax({
        url: "ajax/dataabsen",
        method: "POST",
        data: {
          'mulai': mulai,
          'akhir': akhir,
        },
        dataType: "json"
      });

      request.done(function( datahasil ) {
        $.each(datahasil,function(index,data){
          var tgl = new Date(data["Tgl"]);
          tgl = tgl.getFullYear()+'-'+(tgl.getMonth()+1)+'-'+tgl.getDate();
          data["On_Duty"]=(data["On_Duty"]!=null)?data["On_Duty"]:0;
          data["Off_Duty"]=(data["Off_Duty"]!=null)?data["Off_Duty"]:0;
          var newevent = {
            id: 'absen-'+data["Tgl"],
            title: data["On_Duty"].toString().substr(0,5)+' - '+data["Off_Duty"].toString().substr(0,5),
            textEscape: false,
            start:tgl,
            end: tgl,
            className: 'info',
            url: 'javascript:detailabsen('+data["id"]+');'
          };
          $('#calendar').fullCalendar('renderEvent', newevent, true);
        });
        monthloaded2.push(month+'-'+year);
        getevents(month,year);
      });

      request.fail(function( jqXHR, textStatus ) {
        alert( "Request failed: " + textStatus );
      });
  }
}
window.addEventListener('DOMContentLoaded', (event) => {
  var calendar =  $('#calendar').fullCalendar({
    header: {
      left: 'title',
      center: '', //'agendaDay,agendaWeek,month'
      right: 'prev,next today'
    },
    editable: true,
    firstDay: 0, //  1(Monday) this can be changed to 0(Sunday) for the USA system
    selectable: true,
    defaultView: 'month',

    axisFormat: 'h:mm',
    columnFormat: {
              month: 'ddd',    // Mon
              week: 'ddd d', // Mon 7
              day: 'dddd M/d',  // Monday 9/7
              agendaDay: 'dddd d'
          },
          titleFormat: {
              month: 'MMMM yyyy', // September 2009
              week: "MMMM yyyy", // September 2009
              day: 'MMMM yyyy'                  // Tuesday, Sep 8, 2009
          },
    allDaySlot: false,
    selectHelper: false,
    select: function(start, end, allDay) {
      var title = prompt('Event Title:');
      if (title) {
        calendar.fullCalendar('renderEvent',
          {
            title: title,
            start: start,
            end: end,
            allDay: allDay
          },
          true // make the event "stick"
        );
      }
      calendar.fullCalendar('unselect');
    },
    droppable: true, // this allows things to be dropped onto the calendar !!!
    drop: function(date, allDay) { // this function is called when something is dropped

      // retrieve the dropped element's stored Event Object
      var originalEventObject = $(this).data('eventObject');

      // we need to copy it, so that multiple events don't have a reference to the same object
      var copiedEventObject = $.extend({}, originalEventObject);

      // assign it the date that was reported
      copiedEventObject.start = date;
      copiedEventObject.allDay = allDay;

      // render the event on the calendar
      // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
      $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

      // is the "remove after drop" checkbox checked?
      if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
      }

    },

    events: [

    ],
    eventRender: function (event, element) {
        element.find('.fc-event-title').html(event.title);
        var totalizin =0;
        var start_date = new Date(event.start);
        var end_date = new Date(event.end);
        totalizin+=jmlHariKerja(start_date,end_date);
        $('.totalizin').html(totalizin+' Hari');
    },
    viewRender: function(view, element) {
      $('.totalizin').html(0+' Hari');
      var b = $('#calendar').fullCalendar('getDate');
      getdataabsen(b.getMonth()+1,b.getFullYear());
    }
  });
});
function detailload(id) {
  var idx = dataleave.findIndex(x => x.id == id);
  var start_date = new Date(dataleave[idx]['start_date']);
  var end_date = new Date(dataleave[idx]['end_date']);
  var diff_date = jmlHariKerja(start_date,end_date);
  end_date = end_date.getDate()+' '+monthNames[end_date.getMonth()]+' '+end_date.getFullYear();
  start_date = start_date.getDate()+' '+monthNames[start_date.getMonth()]+' '+start_date.getFullYear();
  $('#popupdetail .modal-body .jenis').html(dataleave[idx]['jenis']);
  $('#popupdetail .modal-body .start_date').html(start_date);
  $('#popupdetail .modal-body .end_date').html(end_date);
  $('#popupdetail .modal-body .diff_date').html(diff_date);
  $('#popupdetail .modal-body .keterangan').html(dataleave[idx]['keterangan']);
  $('#popupdetail').modal();
}
</script>
