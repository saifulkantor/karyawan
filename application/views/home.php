<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
$listkpi = $this->kpi_model->getAll($this->session->id_karyawan,$tahun);
$listabs = $this->abs_model->getCountpermonth($this->session->id_karyawan,$tahun);
$saldocuti = $this->saldocuti_model->get($this->session->id_karyawan,$tahun);
?>

	<form action="" method="get">
		<h4 class="card-title">Tahun <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" style="width: 200px;display: inline-block;"></h4>
	</form>
<div id="body" class="row">
	<div class="font-icon-list col-xs-12 col-sm-6 col-md-4 menu menuperusahaan">
    <div class="font-icon-detail" style="min-height: 100px;padding: 30px;background-color: #9368E9!important;color: white;font-weight: bold;cursor:pointer" onclick="pilihperkiraan(1,'01.A. Hotel SCP')">
			<p>Saldo Cuti</p>
      <h3><?=($saldocuti!=null)?$saldocuti['saldo_cuti']:'0'?></h3>
    </div>
  </div>
	<div style="clear:both;width:100%"></div>
	<div class="col-sm-6">
		<h3>Table KPI</h3>
		<table class="table table-striped table-bordered">
			<tbody>
				<?php
				for ($i=0;$i<12;$i++) {
					$val=array_search($i, array_column($listkpi, 'bln'));
					$val=($val!== false)?$listkpi[$val]['nilai']:'0';
					echo '<tr><td>'.BULAN_FULL[$i].'</td><td>'.$val.'</td></tr>';
				} ?>
			</tbody>
		</table>
	</div>
	<div class="col-sm-6">
		<h3>Absensi</h3>
		<table class="table table-striped table-bordered">
			<tbody>
				<?php
				for ($i=0;$i<12;$i++) {
					$val=array_search($i, array_column($listabs, 'bln'));
					$val=($val!== false)?$listabs[$val]['jumlah']:'0';
					echo '<tr><td>'.BULAN_FULL[$i].'</td><td>'.$val.'</td></tr>';
				} ?>
			</tbody>
		</table>
	</div>
</div>
