<?php
$session = $this->session->userdata();
$pesansession = $this->session->flashdata();
$path=explode("/", uri_string());
$menudipilih=isset($path[0])?$path[0]:'';
$submenudipilih=isset($path[1])?$path[1]:'';
$data=$data!=null?$data:[];
?>
<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>template/creative-team/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="<?=base_url()?>template/creative-team/assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>Karyawan <?=ucfirst($menudipilih)?></title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="<?=base_url()?>assets/css/fonts/montserrat.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/fonts/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="<?=base_url()?>template/creative-team/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?=base_url()?>template/creative-team/assets/css/light-bootstrap-dashboard.css?v=2.0.0 " rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/jquery-ui.css" rel="stylesheet" />
    <!-- Calendar -->
    <link href="<?=base_url()?>assets/css/fullcalendar.css" rel="stylesheet" />
    <!-- <link href="<?=base_url()?>assets/css/fullcalendar.print.css" rel="stylesheet" /> -->
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="<?=base_url()?>template/creative-team/assets/css/demo.css" rel="stylesheet" />
    <link href="<?=base_url()?>assets/css/main.css" rel="stylesheet" />
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-image="<?=base_url()?>template/creative-team/assets/img/sidebar-5.jpg">
            <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

        Tip 2: you can also add an image using data-image tag
    -->
            <div class="sidebar-wrapper">
                <div class="logo">
                    <a href="<?=base_url()?>" class="simple-text">
                        Karyawan
                    </a>
                </div>
                <ul class="nav">
                    <li class="nav-item<?=($menudipilih=='')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>">
                            <i class="nc-icon nc-chart-pie-35"></i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li class="<?=($menudipilih=='dataku')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>dataku">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Dataku</p>
                        </a>
                    </li>
                    <li class="<?=($menudipilih=='kalender')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>kalender">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Kalender</p>
                        </a>
                    </li>
                    <li class="<?=($menudipilih=='gaji')?' active':''?>">
                        <a class="nav-link" href="<?=base_url()?>gaji/slip">
                            <i class="nc-icon nc-paper-2"></i>
                            <p>Slip Gaji</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg " color-on-scroll="500">
                <div class="container-fluid">
                  <a href="<?=base_url()?>"> Dashboard </a>
                    <button href="" class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                        <span class="navbar-toggler-bar burger-lines"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-end" id="navigation">
                        <ul class="nav navbar-nav mr-auto">

                        </ul>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="<?=base_url()?>dataku?tab=akun">
                                    <span class="no-icon">Account</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="<?=base_url()?>auth/logout">
                                    <span class="no-icon">Log out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <!-- End Navbar -->
            <div class="content">
                <div class="container-fluid">
                  <?php $this->load->view($halaman,$data); ?>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <nav>
                        <ul class="footer-menu">

                        </ul>
                        <p class="copyright text-center">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            <a href="#">IT Team</a>, made with love for a better web
                        </p>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="<?=base_url()?>template/creative-team/assets/js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>template/creative-team/assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="<?=base_url()?>template/creative-team/assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<!-- Jquery UI -->
<script src="<?=base_url()?>assets/js/jquery-ui.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/bootstrap-switch.js"></script>
<!--  Chartist Plugin  -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/chartist.min.js"></script>
<!--  Notifications Plugin    -->
<script src="<?=base_url()?>template/creative-team/assets/js/plugins/bootstrap-notify.js"></script>
<!-- Control Center for Light Bootstrap Dashboard: scripts for the example pages etc -->
<script src="<?=base_url()?>template/creative-team/assets/js/light-bootstrap-dashboard.js?v=2.0.0 " type="text/javascript"></script>
<script src="<?=base_url()?>assets/js/Chart.min.js"></script>
<script src="<?=base_url()?>assets/js/utils.js"></script>
<!-- Calendar -->
<script src="<?=base_url()?>assets/js/fullcalendar.js"></script>
<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
<script src="<?=base_url()?>template/creative-team/assets/js/demo.js"></script>
<script src="<?=base_url()?>assets/js/main.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
        //demo.initDashboardPageCharts();

        <?php if (isset($pesansession['errLoginMsg'])) {
          echo 'demo.showNotification("'.$pesansession['errLoginMsg'].'")';
        } ?>

    });
</script>

</html>
