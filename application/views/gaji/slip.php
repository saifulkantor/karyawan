<?php $tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y'); ?>
<div class="row">
   <h1>Slip Gaji</h1>

	<div id="body" style="width:100%">
    <form action="" method="get">
      <div class="col-xs-6 col-md-4" style="float:left">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
      </div>
      <div style="clear:both"></div>
    </form>
    <table class="table table-hover table-striped table-bordered" style="margin-top:20px">
      <tbody>
      <?php
      for ($i=1; $i <= 12; $i++) {
        echo '<tr><td>'.BULAN_FULL[$i-1].'</td><td><span onclick="lihatslip('.$i.',\''.BULAN_FULL[$i-1].'\')" style="cursor:pointer"><i class="fa fa-eye"></i> Lihat Slip</span></td></tr>';
      } ?>
    </tbody>
  </table>
  </div>
</div>

<div id="popupslip" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document" style="-webkit-transform: translate(0, 30px); -o-transform: translate(0, 30px); transform: translate(0, 30px);">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Slip Gaji</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="tableslip">
          <thead>
            <tr><th>Keterangan</th><th>Jumlah</th></tr>
          </thead>
          <tbody>

          </tbody>
        </table>
        <div class="infoslip"> Slip Gaji Belum Ada</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary cetakslip" onclick="cetakslip()">Print</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var dataslip = <?=json_encode($data['slip'])?>;
function lihatslip(bln=0,namabulan='') {
  var data = dataslip.filter(data => data.bln == bln);
  var html = '';
  $.each(data[0],function(index,value){
    html+='<tr><td>'+index+'</td><td>'+value+'</td></tr>';
  });
  $('#popupslip .tableslip tbody').html(html);
  $('#popupslip .modal-title').html('Slip Gaji '+namabulan+' <?=$tahun?>');
  if (html=='') {
    $('#popupslip .tableslip').hide();
    $('#popupslip .cetakslip').hide();
    $('#popupslip .infoslip').show();
  } else {
    $('#popupslip .tableslip').show();
    $('#popupslip .cetakslip').show();
    $('#popupslip .infoslip').hide();
  }
  $('#popupslip').modal();
}
function cetakslip(){
  var html = $('#popupslip .modal-body').html();
  var w = window.open('','printwindow');
  w.document.write("<html><head><title>Slip Gaji</title></head><body>" );
  w.document.write(html);
  w.document.write("</body></html>");
  w.document.close();
  w.focus();
  setTimeout(function() {
      w.print();
      w.close();
  }, 100);
}
</script>
