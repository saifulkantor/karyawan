<?php
$bulan = isset($_GET['bulan'])?$_GET['bulan']:date('m');
$tahun = isset($_GET['tahun'])?$_GET['tahun']:date('Y');
 ?>
<div class="row">
   <h1>Slip Gaji</h1>

	<div id="body" style="width:100%">
    <form action="" method="get">
      <div class="col-xs-6 col-md-4" style="float:left">
        <label>Bulan</label>
        <input type="number" class="form-control" name="bulan" onchange="this.form.submit()" value="<?=$bulan?>" >
      </div>
      <div class="col-xs-6 col-md-4" style="float:left">
        <label>Tahun</label>
        <input type="number" class="form-control" name="tahun" onchange="this.form.submit()" value="<?=$tahun?>" >
      </div>
      <div style="clear:both"></div>
    </form>
    <table class="table table-hover table-striped table-bordered" style="margin-top:20px">
      <tbody>
      <?php if ($data['slip']!=null){
          foreach ($data['slip'] as $key => $value) {
          echo '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
        }
      } else { echo '<tr><td colspan="2">Slip Gaji Belum Ada</td></tr>'; }
      ?>
    </tbody>
  </table>
  </div>
</div>

<script type="text/javascript">

</script>
