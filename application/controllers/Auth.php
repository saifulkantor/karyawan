<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function loadview($halaman='index',$data=array()) {
		$this->load->view('auth/layout',['halaman'=>$halaman,'data'=>$data]);
	}

	public function index()
	{
		if (isset($this->session->id_user) && $this->session->id_user > -1) {
			redirect('');
		} else {
			//$this->session->sess_destroy();
			$this->loadview('auth/form_login', array('errMsg' => $this->session->flashdata('errLoginMsg')));
		}
	}

	public function register()
	{
		if (isset($this->session->id_user) && $this->session->id_user > -1) {
			redirect('');
		} else {
			$this->load->model('karyawan_model');
			$this->load->model('datakaryawan_model');
			$this->load->model('level_model');
			$data = $this->karyawan_model->masukkandata($this->input->post());
			$datakaryawan = $this->datakaryawan_model->masukkandata($this->input->post());
			if (!empty($_REQUEST)) {
				$datausername = $this->karyawan_model->getDuplicate($data['username']);
				$datanik = $this->datakaryawan_model->getDuplicate($datakaryawan['nik']);
				if ($datausername!=null){
					$this->session->set_flashdata('errLoginMsg', 'Username Sudah digunakan');
				} else if ($datanik==null){
					$data['id_user']=0;$datakaryawan['userid']=0;
					if ($this->datakaryawan_model->save($data,$datakaryawan)) {
						$this->session->set_flashdata('errLoginMsg', 'Data sudah berhasil dimasukkan.');
						redirect('');
					}
				} else {
					$this->session->set_flashdata('errLoginMsg', 'NIK Sudah digunakan');
				}
			} else {
				$_POST = array_merge($data,$datakaryawan);
			}
			$_POST['linkimage'] = base_url().'assets/images/karyawan/default.jpg';
			//$this->session->sess_destroy();
			$this->loadview('auth/register', array('errMsg' => $this->session->flashdata('errLoginMsg'),'levelall'=>$this->level_model->getAll()));
		}
	}


	public function login()
	{
		$this->load->model('karyawan_model');

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$do = true;
		if ($username == '') {
			$msg = 'Username tidak boleh kosong';
			$do = false;
		}

		if ($password == '') {
			$msg = 'Password tidak boleh kosong';
			$do = false;
		}

		$timeLogin = date('YmdHis').rand(1,9999);

		if ($do) {
			// get user
			$karyawan = $this->karyawan_model->getbyusername($username);

			if ($karyawan->id_user == '' or $karyawan->id_user == null) {
				$msg = 'User tidak ditemukan';
				$do = false;
			} else {
				if (password_verify($password,$karyawan->password)) {
					$newdata = array(
						'id_user' => $karyawan->id_user,
						'username' => $karyawan->username,
						'hakakses' => $karyawan->hakakses,
						'id_karyawan' => $karyawan->id_karyawan,
						'aktif' => $karyawan->aktif,
						'logged_in' => TRUE,
						'timelogin' => $timeLogin,
						'authkey' => password_hash($karyawan->id_user.$timeLogin, PASSWORD_BCRYPT),
					);
					$this->session->set_userdata($newdata);
					redirect();
				} else {
					$msg = 'Password salah';
					$do = false;
				}
			}
		}

		if (!$do) {
			$this->session->set_flashdata('errLoginMsg', $msg);

			redirect('Auth');
		}
	}

	function generateMenu($menu, $skipAkses = false) {
		$temp = '';
		$i = 1;
		$arrMenu = $arrLaporan = array();
		foreach($menu as $row) {
			$insert = false;
			if ($skipAkses) {
				$insert = true;
			} else if ($row->akses == 1) {
				$insert = true;
			}

			if ($insert == true) {
				if ($i >= 8)
					$i = 1;

				if ($temp <> $row->modul) {
					$temp = $row->modul;
				}

				$row->color = 'c'.$i;

				// apakah menu laporan ?
				$a = explode(' ', $row->title);
				if (in_array('Laporan', $a)) {
					$arrLaporan[$temp][] = $row;
				} else {
					$arrMenu[$temp][] = $row;
				}

				$i++;
			}
		}

		return array('daftarMenu' => $arrMenu, 'daftarLaporan' => $arrLaporan);
	}

	public function logout()
	{
		$this->session->sess_destroy();

		redirect();
	}

	public function cekSession() {
		echo (isset($this->session->idperusahaan) && $this->session->idperusahaan > 0) ? 1 : 0;
	}
}
