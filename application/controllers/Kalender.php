<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kalender extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		if (!empty($_REQUEST)) {
			$this->load->model('abs_model');
			$data = $this->abs_model->masukkandata($this->input->post());
			if ($hasil=$this->abs_model->save($data)) {
				$this->session->set_flashdata('errLoginMsg', 'Data sudah berhasil disimpan');
				if ($_FILES!=null){
					foreach ($_FILES as $key => $file) {
		        $target_path = BASEPATH.'../assets/images/abs/';
		        if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
		        $target_path = $target_path.basename($hasil['id'].'.jpg');

		        move_uploaded_file($file['tmp_name'], $target_path);
		        $target_path = $file['tmp_name'];
					}
				}
			}
		}
		$this->loadview('kalender',array('errMsg' => $this->session->flashdata('errLoginMsg')) );
	}
}
