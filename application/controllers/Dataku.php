<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dataku extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('karyawan_model');
		$this->load->model('datakaryawan_model');
		$this->load->model('level_model');
		if ($this->input->server('REQUEST_METHOD') == 'POST'){
			$dataawal  = $this->karyawan_model->get($this->session->id_user)['data'];
			$datakaryawanawal  = $this->datakaryawan_model->get($dataawal->id_karyawan)['data'];
			$data = $this->karyawan_model->masukkandata($this->input->post());
			$datakaryawan = $this->datakaryawan_model->masukkandata($this->input->post());
			$datausername = $this->karyawan_model->getDuplicate($data['username'],$dataawal->id_user);
			$datanik = $this->datakaryawan_model->getDuplicate($datakaryawan['nik'],$dataawal->id_karyawan);
			if ($datausername!=null) {
				print_r($datausername);
				$this->session->set_flashdata('errLoginMsg', 'Username Sudah digunakan');
			} else if ($datanik==null){
				$data['id_user']=$this->session->id_user;$datakaryawan['doc_id']=$dataawal->id_karyawan;
				$datakaryawan = $this->datakaryawan_model->setallowedchange($datakaryawanawal,$datakaryawan);
				if ($this->datakaryawan_model->save($data,$datakaryawan)) {
					$this->session->set_flashdata('errLoginMsg', 'Data sudah berhasil dimasukkan.');
					if ($this->input->post('listdokpribadihapus')!=null) {
						if ($this->input->post('listdokpribadihapus')!='') {
							$target_path = BASEPATH.'../assets/dokumen/karyawan/pribadi/'.$this->session->id_user.'/';
							$listdokpribadihapus=explode("||",$this->input->post('listdokpribadihapus'));
							foreach ($listdokpribadihapus as $key => $value) {
								if (file_exists($target_path.$value)) {
								   unlink($target_path.$value);
								}
							}
						}
					}
					if ($this->input->post('listdokpendidikanhapus')!=null) {
						if ($this->input->post('listdokpendidikanhapus')!='') {
							$target_path = BASEPATH.'../assets/dokumen/karyawan/pendidikan/'.$this->session->id_user.'/';
							$listdokpendidikanhapus=explode("||",$this->input->post('listdokpendidikanhapus'));
							foreach ($listdokpendidikanhapus as $key => $value) {
								if (file_exists($target_path.$value)) {
								   unlink($target_path.$value);
								}
							}
						}
					}
					if ($_FILES!=null){
						foreach ($_FILES as $key => $file) {
							if ($key=='image') {
								//ftpserver2($file['tmp_name'],'assets/images/karyawan/'.$this->session->id_user.'.jpg');

				        $target_path = BASEPATH.'../assets/images/karyawan/';
				        if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
				        $target_path = $target_path.basename($this->session->id_user.'.jpg');
								move_uploaded_file($file['tmp_name'], $target_path);
								$target_path = $file['tmp_name'];
							} else if ($key=='dokpendidikan1' || $key=='dokpendidikan2' || $key=='dokpendidikan3' || $key=='dokpendidikan4' || $key=='dokpendidikan5') {
								$target_path = BASEPATH.'../assets/dokumen/karyawan/pendidikan/'.$this->session->id_user.'/';
				        if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
								$filecount = 0;
								$files = glob($target_path . "*");
								$filecount = ($files)?count($files):$filecount;
								if ($filecount<5){
					        $target_path = $target_path.basename($file['name']);
									move_uploaded_file($file['tmp_name'], $target_path);
									$target_path = $file['tmp_name'];
								}
							} else if ($key=='dokpribadi1' || $key=='dokpribadi2' || $key=='dokpribadi3' || $key=='dokpribadi4' || $key=='dokpribadi5') {
								$target_path = BASEPATH.'../assets/dokumen/karyawan/pribadi/'.$this->session->id_user.'/';
				        if(!file_exists($target_path)){ mkdir($target_path, '0777', true);}
								$filecount = 0;
								$files = glob($target_path . "*");
								$filecount = ($files)?count($files):$filecount;
								if ($filecount<5){
					        $target_path = $target_path.basename($file['name']);
									move_uploaded_file($file['tmp_name'], $target_path);
									$target_path = $file['tmp_name'];
								}
							}
						}
					}
					redirect('dataku');
				}
			} else {
				$this->session->set_flashdata('errLoginMsg', 'NIK Sudah digunakan');
			}
		} else {
			$data = $this->karyawan_model->get($this->session->id_user);
			if (!$data){redirect('');}
			$datakaryawan = $this->datakaryawan_model->get($data['data']->id_karyawan);
			$data=(array)$data['data'];
			$datakaryawan=(array)$datakaryawan['data'];
			unset($data['nik']);
			unset($data['password']);
			$_POST = array_merge($data,$datakaryawan);
			// print_r($datakaryawan);
			$target_path = BASEPATH.'../assets/images/karyawan/'.$this->session->id_user.'.jpg';
			if(file_exists($target_path)){
				$_POST['linkimage'] = base_url().'assets/images/karyawan/'.$this->session->id_user.'.jpg';
			} else {
				$_POST['linkimage'] = base_url().'assets/images/karyawan/default.jpg';
			}
		}

		$this->loadview('karyawan/data',array('errMsg' => $this->session->flashdata('errLoginMsg'),'levelall'=>$this->level_model->getAll()));
	}
}
