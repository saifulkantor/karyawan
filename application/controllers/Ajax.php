<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function kalender_izin()
	{
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		if ($month==0) {
      echo "Bulan harap diisi";
		} else {
			$this->load->model('datakaryawan_model');
			$dataleave = array();
      $leave = $this->datakaryawan_model->leave($month,$year,$this->session->id_karyawan);
      foreach ($leave as $key => $data) {
        if(file_exists(realpath('assets/img/faces/'.$data['id_emp'].'.jpg')))
        {
          $data['image'] = './assets/img/faces/thumb/'.$data['id_emp'].'.jpg' ;
        }else{
          $data['image'] = './assets/img/faces/default.png';
        }
				$dataleave[]=[
          'id'=>$data['doc_id'],
          'keterangan'=>$data['keterangan'],
          'jenis'=>'Cuti',
					'start_date'=>$data['start_date'],
					'end_date'=>$data['end_date'],

          // 'image'=>$data['image'],
        ];
      }
      echo json_encode($dataleave);
    }
	}
	public function dataabsen() {
		$this->load->model('datakaryawan_model');
		$datakaryawan = $this->datakaryawan_model->get($this->session->id_karyawan)['data'];
		$nip = $datakaryawan->userid;
		$mulai = $this->input->post('mulai');
		$akhir = $this->input->post('akhir');
		$this->load->model('absen_model');
		$dataabsen = $this->absen_model->getAll($nip,$mulai,$akhir);
		echo json_encode($dataabsen);
	}
	public function dataabs() {
		$mulai = $this->input->post('mulai');
		$akhir = $this->input->post('akhir');
		$this->load->model('abs_model');
		$dataabsen = $this->abs_model->getAll($this->session->id_karyawan,$mulai,$akhir);
		echo json_encode($dataabsen);
	}
}
