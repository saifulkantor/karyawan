<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gaji extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function slip()
	{
		$id_karyawan = $this->session->id_karyawan;
		$tahun = $this->input->get('tahun')??date('Y');
		$this->load->model('gaji_model');
		$slip = $this->gaji_model->slippertahun($id_karyawan,$tahun);
		$this->loadview('gaji/slip',array('errMsg' => $this->session->flashdata('errLoginMsg'),'slip'=>$slip) );
	}
}
