<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class level_model extends MY_Model{

    function __construct() {
        parent::__construct();
				$this->table = '[level]';
				$this->id = 'doc_id';
				$this->kode = 'kode';
				$this->kolom = array(
					'kode' => '-',
					'keterangan' => '',
				);
		}

}
