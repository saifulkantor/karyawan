<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class abs_model extends MY_Model{

  function __construct() {
      parent::__construct();
      $this->table = '[abs]';
      $this->id = 'doc_id';
      $this->kode = 'id_emp';
      $this->kolom = array(
          'doc_id'=>0,
          'id_emp'=>$this->session->id_karyawan,
          'status'=>'izin',
          'keterangan'=>'-',
          'tgl'=> date('Y-m-d'),
          'approve'=>0,
      );
	}

	public function getCountpermonth($id_karyawan=0,$tahun=0){
		$sql = "select count({$this->id}) as jumlah, MONTH(tgl) as bln from {$this->table} where id_emp=".$id_karyawan." and YEAR(tgl)=".$tahun." group by MONTH(tgl)";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

  public function getAll($id_karyawan=0,$mulai=0,$akhir=0){
		$sql = "select * from {$this->table} where id_emp=".$id_karyawan." and tgl BETWEEN '".$mulai."'  AND '".$akhir."' ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function getperbulan($id_karyawan=0,$bulan=0,$tahun=0){
		$sql = "select * from {$this->table} where id_emp=".$id_karyawan." and MONTH(tgl)=".$bulan."  and YEAR(tgl)=".$tahun." ";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

	public function grid($paging, $filter){
		if (!isset($paging['sort'])) {
			$paging['sort'] = $this->id;
		}

		$json = array('total' => 0, 'rows' => array());

		$tempSql = $this->id.', username, aktif';

		$filter['sql'] = generateFilter($filter['sql'], $tempSql);

		// dapatkan total terlebih dahulu
		$sql = "select count({$this->id}) as total
				from {$this->table}
				where 1=1 {$filter['sql']}";
		$query = $this->db->query($sql, $filter['param']);
		$json['total'] = $query->row()->total;

		// replace dengan kolom yg ditentukan
		$sql = str_replace("count({$this->id}) as total", $tempSql, $sql);

		// tambahkan paging
		$sql .= " order by {$paging['sort']} {$paging['order']}
				  offset {$paging['offset']} rows fetch next {$paging['rows']} rows only";
		$query = $this->db->query($sql, $filter['param']);
		$json['rows'] = $query->result();

		return $json;
	}


	public function delete($id){
		$sql = "delete from {$this->table} where {$this->id} = ?";
		$query = $this->db->query($sql, $id);

		if ($query) {
			return generateMessage(true);
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Error', 'error');
		}
	}

}
