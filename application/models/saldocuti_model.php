<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class saldocuti_model extends MY_Model{

  function __construct() {
      parent::__construct();
      $this->table = '[cuti]';
      $this->id = 'id_emp';
      $this->kode = 'id_emp';
	}

  // digunakan ketika login
	public function get($id_karyawan=0,$tahun=''){
		$sql = "select * from {$this->table} where id_emp=".$id_karyawan." and YEAR(mulai)=".$tahun." and YEAR(hingga)=".$tahun."";
		$query = $this->db->query($sql);
		return $query->row_array();
	}

	public function grid($paging, $filter){
		if (!isset($paging['sort'])) {
			$paging['sort'] = $this->id;
		}

		$json = array('total' => 0, 'rows' => array());

		$tempSql = $this->id.', username, aktif';

		$filter['sql'] = generateFilter($filter['sql'], $tempSql);

		// dapatkan total terlebih dahulu
		$sql = "select count({$this->id}) as total
				from {$this->table}
				where 1=1 {$filter['sql']}";
		$query = $this->db->query($sql, $filter['param']);
		$json['total'] = $query->row()->total;

		// replace dengan kolom yg ditentukan
		$sql = str_replace("count({$this->id}) as total", $tempSql, $sql);

		// tambahkan paging
		$sql .= " order by {$paging['sort']} {$paging['order']}
				  offset {$paging['offset']} rows fetch next {$paging['rows']} rows only";
		$query = $this->db->query($sql, $filter['param']);
		$json['rows'] = $query->result();

		return $json;
	}


	public function delete($id){
		$sql = "delete from {$this->table} where {$this->id} = ?";
		$query = $this->db->query($sql, $id);

		if ($query) {
			return generateMessage(true);
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Error', 'error');
		}
	}

}
