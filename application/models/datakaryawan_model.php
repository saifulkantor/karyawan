<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class datakaryawan_model extends MY_Model{

    function __construct()
	{
        parent::__construct();
        if (isset($this->session->id_user) == false or $this->session->id_user < 0 or $this->session->aktif <= 0) {
  				$this->table = '[ck]';
        } else {
          $this->table = '[k001]';
        }
				$this->id = 'doc_id';
				$this->kode = 'nik';
				$this->kolom = array(
					'dept' => '',
					'nama_karyawan' => '',
					'jabatan' => '',
					'jml' => '1',
					'level' => '',
					'nip' => '',
					'tgl_masuk' => date('Y-m-d'),
					'status_pegawai' => '',
					'sk' => '',
					'mulai' => date('Y-m-d'),
					'akhir' => date('Y-m-d'),
					'[kontrak ke]' => '1',
					'jenjang' => '',
					'Institusi' => '',
					'jurusan' => '',
					'nilai' => '0',
					'kota_asal' => '',
					'tgl_lahir' => date('Y-m-d'),
					'gender' => '',
					'agama' => '',
					'status' => '',
					'nik' => '0',
					'npwp' => '0',
					'kk' => '0',
					'telp' => '0',
					'alamat' => '',
					'kota' => '',
					'jml_anak' => 0,
					'nama_pasangan' => '',
					'tgl_lahir_pasangan' => date('Y-m-d'),
					'anak1' => '',
					'tgl1' => date('Y-m-d'),
					'anak2' => '',
					'tgl2' => date('Y-m-d'),
					'anak3' => '',
					'tgl3' => date('Y-m-d'),
					'nama_ibu' => '',
					'tgl4' => date('Y-m-d'),
					'remark' => 0,
					'url' => '',
				);
	}

  public function getfromnik($nik){
    $sql = "select a.*
        from {$this->table} a
        where a.nik = ?";
    $query = $this->db->query($sql, $nik);
    if ($query) {
      $msg = generateMessage(true);
      $msg['data'] = $query->row();
      return $msg;
    } else {
      $err = $this->db->error();
      return generateMessage(false, $err['message'], 'Peringatan', 'error');
    }
  }


  public function leave($month,$year,$id_karyawan) {
    $sql = "select l.*, k.nama_karyawan from leave l
      LEFT OUTER JOIN {$this->table} k ON l.id_emp=k.doc_id
      WHERE k.tgl_resign IS NOT NULL AND (MONTH(start_date)=".$month." and YEAR(start_date)=".$year.") OR (MONTH(end_date)=".$month." and YEAR(start_date)=".$year.") and k.{$this->id}=".$id_karyawan;
    $query = $this->db->query($sql);
		return $query->result_array();
  }

  public function setallowedchange($datakaryawanawal,$datakaryawan) {
     $dataarray = (array) $datakaryawanawal;
     $datakaryawan['level'] = $datakaryawanawal->level;
     $datakaryawan['nip'] = $datakaryawanawal->nip;
     $datakaryawan['status_pegawai'] = $datakaryawanawal->status_pegawai;
     $datakaryawan['sk'] = $datakaryawanawal->sk;
     $datakaryawan['[kontrak ke]'] = $dataarray['kontrak ke'];
     $datakaryawan['tgl_masuk'] = $datakaryawanawal->tgl_masuk;
     $datakaryawan['mulai'] = $datakaryawanawal->mulai;
     $datakaryawan['akhir'] = $datakaryawanawal->akhir;
     $datakaryawan['jabatan'] = $datakaryawanawal->jabatan;
     $datakaryawan['dept'] = $datakaryawanawal->dept;
     return $datakaryawan;
  }

  public function save($data = NULL, $datatambahan = NULL) {
		// start trans
		$this->db->trans_begin();
    $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
    $datatambahan['doc_id']=$datatambahan['doc_id']??0;
    $datauser = $this->get($datatambahan['doc_id'])['data'];
    if ($datauser==null) {
      unset($datatambahan[$this->id]);
  		$sql = generateSqlInsert($this->table, $datatambahan);
      $query = $this->db->query($sql['sql'], $sql['param']);
  		$query = $this->db->query('select @@IDENTITY as last_id');
  		$id_karyawan = $query->row()->last_id;
    } else {
      $id_karyawan = $datauser->doc_id;
      unset($datatambahan[$this->id]);
      $sql = generateSqlUpdate($this->table, $datatambahan, array($this->id=>$id_karyawan));
      $query = $this->db->query($sql['sql'], $sql['param']);
    }
    $data['id_karyawan'] = $id_karyawan;

		// insert datakaryawan
		if (count($data) > 0) {
  		$msg = $this->insertUpdateKaryawan($data);
			if (!$msg['success']) {
				// rollback
				$this->db->trans_rollback();

				return $msg;
			}
		}

		if ($this->db->trans_status() === FALSE) {
			// rollback
			$this->db->trans_rollback();

			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Peringatan', 'error');
		} else {
			// commit
			$this->db->trans_commit();

			return generateMessage(true);
		}
	}

  	function insertUpdateKaryawan($detail){
  		$CI =& get_instance();
     	$CI->load->model('karyawan_model');
  		$CI->karyawan_model->save($detail);

  		return generateMessage(true);
  	}


	// combogrid
	public function cgrid($q){
		$tempSql = '';
		$sql = "select {$this->id}, username
				from {$this->table}
				where aktif = 1 and
					  username like ?
					  {$tempSql}";
		$query = $this->db->query($sql, array('%'.$q.'%'));

		return $query->result();
	}

	public function delete($id){
		$sql = "delete from {$this->table} where {$this->id} = ?";
		$query = $this->db->query($sql, $id);

		// delete detail
		// $this->db->query("delete from m_user_akses where {$this->id} = ?", $id);
		// $this->db->query("delete from m_user_perusahaan where {$this->id} = ?", $id);
		// $this->db->query("delete from m_user_gudang where {$this->id} = ?", $id);

		if ($query) {
			return generateMessage(true);
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Error', 'error');
		}
	}
}
