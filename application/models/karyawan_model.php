<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class karyawan_model extends MY_Model{

  function __construct() {
      parent::__construct();
			$this->table = '[user]';
			$this->id = 'id_user';
			$this->kode = 'username';
			$this->kolom = array(
        'id_user' => '0',
				'username' => '-',
				'password' => '-',
				'hakakses' => 'karyawan',
				'id_karyawan' => '0',
				'aktif' => 0,
			);
	}

  // digunakan ketika login
	public function getbyusername($username=''){
		$sql = "select a.*
				from {$this->table} a
				where a.username = ?";
		$query = $this->db->query($sql, $username);
		return $query->row();
	}

	public function grid($paging, $filter){
		if (!isset($paging['sort'])) {
			$paging['sort'] = $this->id;
		}

		$json = array('total' => 0, 'rows' => array());

		$tempSql = $this->id.', username, aktif';

		$filter['sql'] = generateFilter($filter['sql'], $tempSql);

		// dapatkan total terlebih dahulu
		$sql = "select count({$this->id}) as total
				from {$this->table}
				where 1=1 {$filter['sql']}";
		$query = $this->db->query($sql, $filter['param']);
		$json['total'] = $query->row()->total;

		// replace dengan kolom yg ditentukan
		$sql = str_replace("count({$this->id}) as total", $tempSql, $sql);

		// tambahkan paging
		$sql .= " order by {$paging['sort']} {$paging['order']}
				  offset {$paging['offset']} rows fetch next {$paging['rows']} rows only";
		$query = $this->db->query($sql, $filter['param']);
		$json['rows'] = $query->result();

		return $json;
	}


	public function delete($id){
		$sql = "delete from {$this->table} where {$this->id} = ?";
		$query = $this->db->query($sql, $id);

		if ($query) {
			return generateMessage(true);
		} else {
			$err = $this->db->error();
			return generateMessage(false, $err['message'], 'Error', 'error');
		}
	}

}
