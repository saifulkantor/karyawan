<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// generate sql insert
function generateSqlInsert($table, $data) {
    // get keys
    $keys = array_keys($data);

    // get values
    $values = array_values($data);

    $sqlValues = str_repeat(',?', count($data));

    return array('sql'=> 'insert into '.$table.' ('.implode(',', $keys).') values (\''.implode('\',\'', $values).'\');', 'param'=> $values);
}

// generate sql insert multiple values
function generateSqlInsertBatch($table, $data) {
    $i = 0;
    $values = array();
    foreach($data as $header) {
        if ($i == 0) {
            // get keys
            $keys = array_keys($header);

            $b = str_repeat(',?', count($header));
            $c = '('.substr($b, 1).')';
        }

        // get values
        $values = array_merge($values, array_values($header));

        $i++;
    }

    $sqlVal = str_repeat(','.$c, count($data));

    return array('sql'=> 'insert into '.$table.' ('.implode(',', $keys).') values '.substr($sqlVal, 1).';', 'param'=> $values);
}

// generate sql update
function generateSqlUpdate($table, $data, $where){
    // get keys
    $keys['set'] = array_keys($data);
    $keys['where'] = array_keys($where);

    // get values
    $values = array_merge(array_values($data), array_values($where));

    $set = implode(',', array_map(function ($a, $b) { return "$a = '$b'"; }, array_keys($data),array_values($data)));
    $setwhere = implode(',', array_map(function ($a, $b) { return "$a = '$b'"; }, array_keys($where),array_values($where)));

    // return array('sql'=> 'update '.$table.' set '.implode('=?,', $keys['set']).'=? where '.implode('=? and ', $keys['where']).'=?', 'param'=> $values);
    return array('sql'=> 'update '.$table.' set '.$set.' where '.$setwhere.'', 'param'=> $values);
}

// digunakan ketika filter datagrid
function generateFilter($sqlSearch, $sqlField){
    /*$sqlSearch = 'id_jual like ? and amount_pajak > ?';

    $sqlField = 'a.id_jual, a.kode_jual, d.kode_so, a.tgl_trans, b.nama_customer,
        c.kode_gudang, c.nama_gudang, a.amount_dpp, a.amount_pajak, a.amount_biaya,
        a.amount_uangmuka, a.amount_grandtotal, a.status, a.catatan, a.auth_key';*/

    // trim space
	$str_explode = explode(',', trim($sqlField));
	foreach($str_explode as $item) {
		$temp = explode(' as ', $item);
		if (count($temp) > 1) {
			// replace
			$sqlSearch = str_replace(trim($temp[1]), trim($temp[0]), $sqlSearch);
		}
    }

    // jika tidak ada huruf '.', maka tidak perlu difilter
    if (strpos($sqlField, '.') == false) {
        return $sqlSearch;
    }
    // trim space, remove space and explode per field
    $str_explode = explode(',', preg_replace('/\s+/', '', trim($sqlField)));
    foreach($str_explode as $item) {
        // penggal string *.
        $tempStr = substr($item, 2);

        // replace
        $sqlSearch = str_replace($tempStr, $item, $sqlSearch);
    }

    return $sqlSearch;
}
